-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 19, 2017 at 09:01 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel_lounge`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` bigint(20) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `line1` varchar(255) DEFAULT NULL,
  `line2` varchar(255) DEFAULT NULL,
  `pincode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `city`, `country`, `state`, `landmark`, `line1`, `line2`, `pincode`) VALUES
(1, 'Mira road', 'India', 'Maharashtra', 'Shantinagar', 'Flat No204 c-38 Sector*8', NULL, '401107');

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE `birthdate` (
  `id` int(11) NOT NULL,
  `bdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` bigint(20) NOT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `fax_no` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `tel_no` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `email_id`, `fax_no`, `mobile_no`, `tel_no`, `website_url`) VALUES
(1, 'sujit@gmail.com', '7896541230', '789654230', '74185296', 'sj@jd.com');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` bigint(20) NOT NULL,
  `service_details` varchar(255) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_details`, `service_name`) VALUES
(1, 'Udd Jao', 'Flight'),
(2, 'Zameeen Se jao lekin aaram se jao', 'Train'),
(3, 'tabdak tabdak', 'Ghoda Gaadi'),
(4, 'tabdak tabdak', 'Ghoda Gaadi');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_info`
--

CREATE TABLE `transaction_info` (
  `info_id` bigint(20) NOT NULL,
  `adult_count` int(11) DEFAULT NULL,
  `child_count` int(11) DEFAULT NULL,
  `depart_date` datetime DEFAULT NULL,
  `infant_count` int(11) DEFAULT NULL,
  `journey_from` varchar(255) DEFAULT NULL,
  `journey_to` varchar(255) DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  `transactionMaster_id` bigint(20) DEFAULT NULL,
  `travel_class` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_info`
--

INSERT INTO `transaction_info` (`info_id`, `adult_count`, `child_count`, `depart_date`, `infant_count`, `journey_from`, `journey_to`, `return_date`, `transactionMaster_id`, `travel_class`) VALUES
(1, 2, 0, '2017-06-18 00:00:00', 0, 'wey', 'sssss', '2017-06-21 00:00:00', 1, NULL),
(2, 0, 0, NULL, 0, 'gfds', 'hgfds', NULL, 2, NULL),
(3, 2, 0, '2017-06-13 00:00:00', 0, 'vb mnv', 'jvcn j', NULL, 4, NULL),
(4, 1, 0, '2017-06-21 00:00:00', 0, 'Delhi', 'Ladakh', '2017-06-23 00:00:00', 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_master`
--

CREATE TABLE `transaction_master` (
  `id` bigint(20) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `inserted_by` varchar(255) DEFAULT NULL,
  `inserted_date` datetime DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL,
  `status_id` bigint(20) DEFAULT NULL,
  `vendor_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_master`
--

INSERT INTO `transaction_master` (`id`, `customer_name`, `inserted_by`, `inserted_date`, `transaction_id`, `updated_by`, `updated_date`, `service_id`, `status_id`, `vendor_id`) VALUES
(1, NULL, 'Sujit Jadhav', '2017-06-17 16:02:22', NULL, NULL, NULL, 1, 1, 1),
(2, NULL, 'Sujit Jadhav', '2017-06-17 18:47:36', 'ONO213232', NULL, NULL, 1, 1, 1),
(4, NULL, 'Sujit Jadhav', '2017-06-17 20:06:40', 'ONO100000002', NULL, NULL, 1, 2, 1),
(5, NULL, 'Sujit Jadhav', '2017-06-19 00:02:46', 'ONO100000003', NULL, NULL, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_status`
--

CREATE TABLE `transaction_status` (
  `status_id` bigint(20) NOT NULL,
  `tName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_status`
--

INSERT INTO `transaction_status` (`status_id`, `tName`) VALUES
(1, 'Bought'),
(2, 'Sold'),
(3, 'Cancelled'),
(4, 'Service Failed');

-- --------------------------------------------------------

--
-- Table structure for table `traveller_category`
--

CREATE TABLE `traveller_category` (
  `category_id` bigint(20) NOT NULL,
  `cat_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traveller_category`
--

INSERT INTO `traveller_category` (`category_id`, `cat_name`) VALUES
(1, 'Adult'),
(2, 'Child'),
(3, 'Infant');

-- --------------------------------------------------------

--
-- Table structure for table `traveller_info`
--

CREATE TABLE `traveller_info` (
  `traveller_id` bigint(20) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `traveller_name` varchar(255) DEFAULT NULL,
  `transaction_master_id` bigint(20) DEFAULT NULL,
  `traveller_category` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traveller_info`
--

INSERT INTO `traveller_info` (`traveller_id`, `age`, `traveller_name`, `transaction_master_id`, `traveller_category`) VALUES
(1, 22, 'sujit', 5, 1),
(2, 21, 'sssssss', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `travel_class`
--

CREATE TABLE `travel_class` (
  `class_id` bigint(20) NOT NULL,
  `class_details` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `travel_class`
--

INSERT INTO `travel_class` (`class_id`, `class_details`, `class_name`, `service_id`) VALUES
(1, 'Low Fair', 'Economy', 1),
(2, 'Better than Economy', 'Premium Economy', 1),
(3, 'Normal Fair', 'First Class', 1),
(4, 'High Fair', 'Business', 1),
(5, 'free', 'free', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` bigint(20) NOT NULL,
  `vendor_name` varchar(255) DEFAULT NULL,
  `address_id` bigint(20) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `address_id`, `contact_id`) VALUES
(1, 'Sujit Jadhav', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_map_services`
--

CREATE TABLE `vendor_map_services` (
  `vendor_id` bigint(20) NOT NULL,
  `service_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_map_services`
--

INSERT INTO `vendor_map_services` (`vendor_id`, `service_id`) VALUES
(1, 1),
(1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `transaction_info`
--
ALTER TABLE `transaction_info`
  ADD PRIMARY KEY (`info_id`),
  ADD KEY `FK1E52656F3F94E996` (`travel_class`),
  ADD KEY `FK1E52656FB5A0CD1F` (`transactionMaster_id`);

--
-- Indexes for table `transaction_master`
--
ALTER TABLE `transaction_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaction_id` (`transaction_id`),
  ADD KEY `FKD970FEE3E089C3D` (`status_id`),
  ADD KEY `FKD970FEE3E884431F` (`service_id`),
  ADD KEY `FKD970FEE35353FE95` (`vendor_id`);

--
-- Indexes for table `transaction_status`
--
ALTER TABLE `transaction_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `traveller_category`
--
ALTER TABLE `traveller_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `traveller_info`
--
ALTER TABLE `traveller_info`
  ADD PRIMARY KEY (`traveller_id`),
  ADD KEY `FK8233A82E629B961C` (`transaction_master_id`),
  ADD KEY `FK8233A82E1C49AEA0` (`traveller_category`);

--
-- Indexes for table `travel_class`
--
ALTER TABLE `travel_class`
  ADD PRIMARY KEY (`class_id`),
  ADD KEY `FKA66F5013E884431F` (`service_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`),
  ADD KEY `FKCF1EA548E494C6BF` (`address_id`),
  ADD KEY `FKCF1EA5481A1BC73F` (`contact_id`);

--
-- Indexes for table `vendor_map_services`
--
ALTER TABLE `vendor_map_services`
  ADD PRIMARY KEY (`vendor_id`,`service_id`),
  ADD KEY `FK169C7D78E884431F` (`service_id`),
  ADD KEY `FK169C7D785353FE95` (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transaction_info`
--
ALTER TABLE `transaction_info`
  MODIFY `info_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transaction_master`
--
ALTER TABLE `transaction_master`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `transaction_status`
--
ALTER TABLE `transaction_status`
  MODIFY `status_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `traveller_category`
--
ALTER TABLE `traveller_category`
  MODIFY `category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `traveller_info`
--
ALTER TABLE `traveller_info`
  MODIFY `traveller_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `travel_class`
--
ALTER TABLE `travel_class`
  MODIFY `class_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaction_info`
--
ALTER TABLE `transaction_info`
  ADD CONSTRAINT `FK1E52656F3F94E996` FOREIGN KEY (`travel_class`) REFERENCES `travel_class` (`class_id`),
  ADD CONSTRAINT `FK1E52656FB5A0CD1F` FOREIGN KEY (`transactionMaster_id`) REFERENCES `transaction_master` (`id`);

--
-- Constraints for table `transaction_master`
--
ALTER TABLE `transaction_master`
  ADD CONSTRAINT `FKD970FEE35353FE95` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`),
  ADD CONSTRAINT `FKD970FEE3E089C3D` FOREIGN KEY (`status_id`) REFERENCES `transaction_status` (`status_id`),
  ADD CONSTRAINT `FKD970FEE3E884431F` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`);

--
-- Constraints for table `traveller_info`
--
ALTER TABLE `traveller_info`
  ADD CONSTRAINT `FK8233A82E1C49AEA0` FOREIGN KEY (`traveller_category`) REFERENCES `traveller_category` (`category_id`),
  ADD CONSTRAINT `FK8233A82E629B961C` FOREIGN KEY (`transaction_master_id`) REFERENCES `transaction_master` (`id`);

--
-- Constraints for table `travel_class`
--
ALTER TABLE `travel_class`
  ADD CONSTRAINT `FKA66F5013E884431F` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`);

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `FKCF1EA5481A1BC73F` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`),
  ADD CONSTRAINT `FKCF1EA548E494C6BF` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`);

--
-- Constraints for table `vendor_map_services`
--
ALTER TABLE `vendor_map_services`
  ADD CONSTRAINT `FK169C7D785353FE95` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`),
  ADD CONSTRAINT `FK169C7D78E884431F` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
