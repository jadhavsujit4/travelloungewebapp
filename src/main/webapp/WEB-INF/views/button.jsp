<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>
</head>
<body>

<div class="row">
            <div class="col l2">
<ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
      <div class="collapsible-body">
      <span>
          Adult (12+ yrs)
    <form id="f1" style="width:20%;">
        <div class="row">
            <div class="col l2">
                <input type='button' name='add' value='+' />
            </div>
            <div class="col l4" style="padding:0px 8px 0px 25px;">
                <input type='text' name='qty' id='qty' value='0' />
            </div>
            <div class="col l2">
                <input type='button' name='subtract' value='-' />
            </div>
        </div>
    </form>
    Children (2-12 yrs)
    <form id="f2" style="width:20%;">
        <div class="row">
            <div class="col l2">
                <input type='button' name='add' value='+' />
            </div>
            <div class="col l4" style="padding:0px 8px 0px 25px;">
                <input type='text' name='qty' id='qty' value='0' />
            </div>
            <div class="col l2">
                <input type='button' name='subtract' value='-' />
            </div>
        </div>
    </form>
    Infant(s) (below 2 yrs)
    <form id="f3" style="width:20%;">
        <div class="row">
            <div class="col l2">
                <input type='button' name='add' value='+' />
            </div>
            <div class="col l4" style="padding:0px 8px 0px 25px;">
                <input type='text' name='qty' id='qty' value='0' />
            </div>
            <div class="col l2">
                <input type='button' name='subtract' value='-' />
            </div>
        </div>
    </form>
      </span>
      </div>
    </li>
    </ul>
    <ul>
    <li>
    </li>
    </ul>
    Adult (12+ yrs)
    <form id="f1" style="width:20%;">
        <div class="row">
            <div class="col l6">
                <input type='button' name='add' value='+' />
           <!--  </div>
            <div class="col l4" style="padding:0px 8px 0px 25px;"> -->
                <input type='text' name='qty' id='qty' value='0' />
            <!-- </div>
            <div class="col l2"> -->
                <input type='button' name='subtract' value='-' />
            </div>
        <!-- </div>
    </form>
    </li>
    <li> -->
    Children (2-12 yrs)
    <form id="f2" style="width:20%;">
        <!-- <div class="row"> -->
            <div class="col l6">
                <input type='button' name='add' value='+' />
            <!-- </div>
            <div class="col l4" style="padding:0px 8px 0px 25px;"> -->
                <input type='text' name='qty' id='qty' value='0' />
           <!--  </div>
            <div class="col l2"> -->
                <input type='button' name='subtract' value='-' />
            <!-- </div>
        </div> -->
    </form>
   <!--  </li>
    <li> -->
    Infant(s) (below 2 yrs)
    <form id="f3" style="width:20%;">
       <!--  <div class="row"> -->
            <div class="col l2">
                <input type='button' name='add' value='+' />
            <!-- </div>
            <div class="col l4" style="padding:0px 8px 0px 25px;"> -->
                <input type='text' name='qty' id='qty' value='0' />
          <!--   </div>
            <div class="col l2"> -->
                <input type='button' name='subtract' value='-' />
            </div>
        </div>
    </form>
    </li>
    </ul>
    <script type="text/javascript">
        with(document.getElementById('f1'))
        incrementButtons(add, subtract, qty);
        with(document.getElementById('f2'))
        incrementButtons(add, subtract, qty);
        with(document.getElementById('f3'))
        incrementButtons(add, subtract, qty);
    </script>
</body>

</html>