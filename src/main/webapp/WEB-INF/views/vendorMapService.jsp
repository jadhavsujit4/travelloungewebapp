<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%-- <%@taglib uri="http://www.springframework.org/tags/form" prefix="fm"%>
 --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<div class="row">
  <form action="mapServiceToVendor" method="post">
    <div class="input-field col s6 m6 l6">
     <h6>Choose Vendor</h6>
      <select class="teal lighten-3" name="vendorId">
        <option value="" disabled selected>Select Vendor</option>
        <c:forEach var="vendor" items="${vendorList}">
				<option value="${vendor.vendorId }">${vendor.vendorName }</option>
			</c:forEach>
      </select>
     
    </div>

    <div class="input-field col s6 m6 l6">
    <h6>Choose Service</h6><br>
    	<c:forEach var="service" items="${serviceList}">
    <input type="checkbox" name="serviceId" value="${service.serviceId }"> 
          <label for="${service.serviceName }">${service.serviceName }</label>
    
		</c:forEach>
     </div>
     <div class="input-field col s6 m6 l4 offset-l5">
					<button class="btn waves-effect waves-light blue darken-4"
						type="submit" name="action">
						Submit <i class="material-icons right">Add Service to Vendor</i>
					</button>
				</div>
    </form>
  </div>



<%-- <fm:form action="mapServiceToVendor" method="post">
		
		<fm:select path="vendorId" items="${vendorList.vendorName }"></fm:select>
		<fm:checkbox path="service_id" value="${serviceList.serviceName }" />



		<select name="vendorId">
			<c:forEach var="vendor" items="${vendorList}">
				<option value="${vendor.vendorId }">${vendor.vendorName }</option>
			</c:forEach>
		</select>

		<c:forEach var="service" items="${serviceList}">
			<input type="checkbox" name="serviceId" value="${service.serviceId }"> ${service.serviceName }
		</c:forEach>
		<input type="submit" value="Add service">
	</fm:form> --%>
</body>
</html>