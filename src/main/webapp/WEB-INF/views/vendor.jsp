<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>

<style type="text/css">
.focus {
	background-color: #0d47a1;
	color: #fff;
	cursor: pointer;
	font-weight: bold;
}

.pageNumber {
	padding: 2px;
}
</style>
</head>
<body>
	<%@include file="components/navbar.jsp"%>
	<!-- main content start-->
	<!-- main content -->
	<main>
	<div class="row">
		<div class="col s2 m2 l2">Enter Keyword here:</div>
		<div class="col s6 m6 l4 teal">
			<form style="width: 350px; height: 80px; padding: 2px 12px 5px 0px;">
				<div class="input-field teal accent-4" style="width: 330px;">
					<input id="search" type="search" placeholder="search..." required>
					<label class="label-icon" for="search"><i
						class="material-icons">search</i></label> <i class="material-icons"
						style="padding-top: 9px;">close</i>
				</div>
			</form>
		</div>
		<!-- <div class="col s6 m6 l6 teal" >
    <input type="text" id="search" placeholder="search"></input>
  </div> -->
		<div class="col s4 m4 l4">
			<a class="waves-effect waves-light btn teal right"
				href="addVendor"><i class="material-icons left">add</i>Add
				New Vendor</a>
		</div>
	</div>
	<table id="tblData" class="striped bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Item Name</th>
				<th>Item Price</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Ana</td>
				<td>Melody</td>
				<td>3.76</td>
			</tr>
			<tr>
				<td>Lana</td>
				<td>KitKAt</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Alan</td>
				<td>Jellybean</td>
				<td>76</td>
			</tr>
			<tr>
				<td>Jonathan</td>
				<td>Lollipop</td>
				<td>7</td>
			</tr>
			<tr>
				<td>Alvin</td>
				<td>Eclair</td>
				<td>0.87</td>
			</tr>
			<tr>
				<td>hello</td>
				<td>choco</td>
				<td>37</td>
			</tr>
			<tr>
				<td>lelo</td>
				<td>choco</td>
				<td>37</td>
			</tr>
			<tr>
				<td>trello</td>
				<td>choco</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Carry</td>
				<td>what</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Larry</td>
				<td>parle</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Larry</td>
				<td>parle</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Larry</td>
				<td>parle</td>
				<td>37</td>
			</tr>
		</tbody>
	</table>
	<div id="pages">
		<span class="pageNumber"></span> <span class="pageNumber"></span> <span
			class="pageNumber"></span> <span class="pageNumber"></span>
	</div>
	</main>

	<!-- main content ends -->
	<!-- main content end -->
	<%@include file="components/footer.jsp"%>
</body>
</html>