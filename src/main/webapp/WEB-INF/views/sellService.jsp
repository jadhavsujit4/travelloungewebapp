<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>
</head>
<body>

	<div class="container">
		<form action="sellingService" style="padding: 0px 20px 0px 20px;" method="post">
			<div class="row">
				<c:forEach var="i" begin="1" end="${transactionInfo.adultCount}">
					<div class="input-field col s12 m2 l2">Adult Name</div>
					<div class="input-field col s3 m3 l1">
						<select>
							<option value="" disabled selected>Title</option>
							<option value="1">Mr.</option>
							<option value="2">Ms.</option>
							<option value="3">Mrs.</option>
							<option value="3">Master</option>
						</select>
					</div>
					<div class="input-field col s9 m3 l3">
						<input placeholder="Name" name="adult_${i}_name" id="adult_first_name"
							type="text" class="validate">
					</div>
					<div class="input-field col s3 m2 l2">Age</div>
					<div class="input-field col s9 m6 l3">
						<input type="text" class="validate" placeholder="Age" name="adult_${i}_age"
							id="child_age">
					</div>
					<div class="input-field col s3 m2 l1">Gender</div>
					<div class="input-field col s9 m6 l3">
						<input name="adult_${i}_gender" type="radio" id="child_male" /> <label
							for="child_male">Male</label> <input name="adult_${i}_gender" type="radio"
							id="child_female" /> <label for="child_female">Female</label>
					</div>
				</c:forEach>
			</div>
			
			<div class="row">
				<c:forEach var="i" begin="1" end="${transactionInfo.childCount}">
					<div class="input-field col s12 m2 l2">Child Name</div>
					<div class="input-field col s3 m3 l1">
						<select>
							<option value="" disabled selected>Title</option>
							<option value="1">Mr.</option>
							<option value="2">Ms.</option>
							<option value="3">Mrs.</option>
							<option value="3">Master</option>
						</select>
					</div>
					<div class="input-field col s9 m3 l3">
						<input placeholder="Name" name="adult_${i}_name" id="child_first_name"
							type="text" class="validate">
					</div>
					<div class="input-field col s3 m2 l2">Age</div>
					<div class="input-field col s9 m6 l3">
						<input type="text" class="validate" placeholder="Age" name="child_${i}_age"
							id="child_age">
					</div>
					<div class="input-field col s3 m2 l1">Gender</div>
					<div class="input-field col s9 m6 l3">
						<input name="child_${i}_gender" type="radio" id="child_male" /> <label
							for="child_male">Male</label> <input name="child_${i}_gender" type="radio"
							id="child_female" /> <label for="child_female">Female</label>
					</div>
				</c:forEach>
			</div>
			
			
			
			<!-- 
			<div class="row">
				<div class="input-field col s12 m6 l6">
					<i class="material-icons prefix">mode_edit</i> <input id="email"
						type="email" class="validate"> <label for="email"
						data-error="wrong" data-success="right">Email</label>
				</div>
				<div class="input-field col s12 m6 l6">
					<i class="material-icons prefix">phone</i> <input
						id="icon_telephone" type="tel" class="validate"> <label
						for="icon_telephone">Telephone</label>
				</div>
			</div> -->

				<div class="input-field col s6 m6 l4 offset-l5">
					<button class="btn waves-effect waves-light black darken-4"
						type="submit">
						Make Payment <i class="material-icons right">send</i>
					</button>
				</div>
		</form>
	</div>
	<%@include file="components/footer.jsp"%>
</body>

</html>