<!DOCTYPE html>
<html lang="en">
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>
</head>
<body>
	<%@include file="components/navbar.jsp"%>
	<!-- main content start-->
	<!-- main content -->
	<main>
	<div class="row">
		<form class="col s12" action="addingService" method="post">
			<div class="row">
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">perm_identity</i> <input id="serviceName" name="serviceName"
						type="text" class="validate"> <label for="serviceName">Name</label>
				</div>

				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">assignment</i>
					<textarea id="serviceDetails" name="serviceDetails" class="materialize-textarea"></textarea>
					<label for="serviceDetails">Details</label>
				</div>
				<div class="input-field col s6 m6 l4 offset-l5">
					<button class="btn waves-effect waves-light blue darken-4"
						type="submit" name="action">
						Submit <i class="material-icons right">send</i>
					</button>
				</div>
			</div>
		</form>
	</div>
	</main>
	<!-- main content ends -->
	<!-- main content end -->
	<%@include file="components/footer.jsp"%>
</body>

</html>