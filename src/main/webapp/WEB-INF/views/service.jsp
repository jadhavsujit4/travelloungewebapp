<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		var totalRows = $('#tblData').find('tbody tr:has(td)').length;
		// this recordperpage tells how many records should be shown in 1 page.
		var recordPerPage = 6;
		// here we divided total tr rows by record per page which will give us total no. of pages to be shown
		var totalPages = Math.ceil(totalRows / recordPerPage);
		// save page numbers in pages variable
		var $pages = $('<div id="pages"></div>');
		// loop for getting page numbers
		for (i = 0; i < totalPages; i++) {
			// span tag to hold page numbers and append it to div tag above
			$('<span>&nbsp;' + (i + 1) + '</span>').appendTo($pages);
		}
		$pages.appendTo('#tblData');
		// hover event to page no.
		$('#pages').hover(function() {
			$(this).addClass('focus');
		}, function() {
			$(this).removeClass('focus');
		});
		$('table').find('tbody tr:has(td)').hide();
		var tr = $('table tbody tr:has(td)');
		for (var i = 0; i <= recordPerPage - 1; i++) {
			$(tr[i]).show();
		}
		$('span').click(function(event) {
			$('#tblData').find('tbody tr:has(td)').hide();
			var nBegin = ($(this).text() - 1) * recordPerPage;
			var nEnd = $(this).text() * recordPerPage - 1;
			for (var i = nBegin; i <= nEnd; i++) {
				$(tr[i]).show();
			}
		});
		$("#search").keyup(function() {
			var value = this.value;
			$("table").find("tr").each(function(index) {
				if (index === 0)
					return;
				var id = $(this).find("td").text();
				$(this).toggle(id.indexOf(value) !== -1);
			});
		});
	});
</script>
</head>
<body>
	<%@include file="components/navbar.jsp"%>
	<!-- main content start-->
	<!-- main content -->
	<main>

	<div class="row">
		<div class="col s2 m2 l2">Enter Keyword here:</div>
		<div class="col s6 m6 l4 teal">
			<form style="width: 350px; height: 80px; padding: 2px 12px 5px 0px;">
				<div class="input-field teal accent-4" style="width: 330px;">
					<input id="search" type="search" placeholder="search..." required>
					<label class="label-icon" for="search"><i
						class="material-icons">search</i></label> <i class="material-icons"
						style="padding-top: 9px;">close</i>
				</div>
			</form>
		</div>
		<div class="col s4 m4 l4">
			<a class="waves-effect waves-light btn teal right"
				href="addService"><i class="material-icons left">add</i>Add
				New Service</a>
		</div>
	</div>

	<table id="tblData" class="striped bordered">
		<thead>
			<tr>
				<th>Service Name</th>
				<th>Details</th>
				<th>Data</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Ana</td>
				<td>Melody</td>
				<td>3.76</td>
			</tr>
			<tr>
				<td>Lana</td>
				<td>KitKAt</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Alan</td>
				<td>Jellybean</td>
				<td>76</td>
			</tr>
			<tr>
				<td>Jonathan</td>
				<td>Lollipop</td>
				<td>7</td>
			</tr>
			<tr>
				<td>Alvin</td>
				<td>Eclair</td>
				<td>0.87</td>
			</tr>
			<tr>
				<td>hello</td>
				<td>choco</td>
				<td>37</td>
			</tr>
			<tr>
				<td>lelo</td>
				<td>choco</td>
				<td>37</td>
			</tr>
			<tr>
				<td>trello</td>
				<td>choco</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Carry</td>
				<td>what</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Larry</td>
				<td>parle</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Larry</td>
				<td>parle</td>
				<td>37</td>
			</tr>
			<tr>
				<td>Larry</td>
				<td>parle</td>
				<td>37</td>
			</tr>
		</tbody>
	</table>
	<div id="pages">
		<span class="pageNumber"></span> <span class="pageNumber"></span> <span
			class="pageNumber"></span> <span class="pageNumber"></span>
	</div>
	</main>
	<!-- main content ends -->
	<!-- main content end -->
	<%@include file="components/footer.jsp"%>
</body>

</html>