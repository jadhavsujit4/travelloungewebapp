<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>
</head>
<body>
	<%@include file="components/navbar.jsp"%>
	<!-- main content start-->
	<!-- main content -->
	<main>
	<div class="container">
		<h3 class="center">Vendor Form</h3> 
		<div class="row">
			<form class="col s12 m12 l12" action="addingVendor" method="post">
				<div class="row">
					<div class="input-field col s6 m6 l6 offset-l3">
						<i class="material-icons prefix">perm_identity</i> <input
							id="vendorName" name="vendorName" type="text" class="validate"> <label for="name">Name</label>
					</div>
					<div class="col l12 m12 s12">
						<h4 class="center">Contact</h4>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">stay_current_portrait</i> <input
							id="mobileNumber" name="mobileNumber" type="tel" class="validate"> <label
							for="mobileNumber">Phone 1</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">stay_current_portrait</i> <input
							id="telephoneNumber" name="telephoneNumber" type="tel" class="validate"> <label
							for="telephoneNumber">Telephone</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">email</i> <input id="emailId" name="emailId"
							type="tel" class="validate"> <label for="email">Email</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">phone</i> <input
							id="telephoneNumber" type="tel" class="validate"> <label
							for="telephoneNumber">Telephone Number</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">tab</i> <input id="faxNumber" name="faxNumber"
							type="tel" class="validate"> <label for="faxNumber">Fax</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">tab</i> <input id=websiteUrl name="websiteUrl"
							type="tel" class="validate"> <label for="websiteUrl">Website</label>
					</div>
				</div>
				<div class="row">
					<div class="col l12 m12 s12">
						<h4 class="center">Address</h4>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">store</i> <input id="line1" name="line1"
							type="text" class="validate"> <label for="line1">Line
							1</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">store</i> <input id="line2" name="line2"
							type="text" class="validate"> <label for="line2">Line
							2</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">location_on</i> <input name="landmark"
							id="landmark" type="text" class="validate"> <label
							for="landmark">Landmark</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">location_on</i> <input id="state" name="addressState"
							type="text" class="validate"> <label for="state">State</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">location_on</i> <input id="city" name="addressCity"
							type="text" class="validate"> <label for="city">City</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">business</i> <input id="pincode" name="pincode"
							type="text" class="validate"> <label for="pincode">pincode</label>
					</div>
					<div class="input-field col s6 m6 l6">
						<i class="material-icons prefix">business</i> <input id="addressCountry" name="addressCountry"
							type="text" class="validate"> <label for="addressCountry">Country</label>
					</div>
				</div>
				<div class="input-field col s12 m12 l12">
					<i class="material-icons prefix">speaker_notes</i>
					<textarea id="comment" class="materialize-textarea"></textarea>
					<label for="comment">Comment</label>
				</div>

				<div class="input-field col s6 m6 l4 offset-l5">
					<button class="btn waves-effect waves-light blue darken-4"
						type="submit" name="action">
						Submit <i class="material-icons right">send</i>
					</button>
				</div>
		</form>
	</div>
	</div>
	</main>
	<!-- main content ends -->
	<!-- main content end -->
	<%@include file="components/footer.jsp"%>
</body>

</html>