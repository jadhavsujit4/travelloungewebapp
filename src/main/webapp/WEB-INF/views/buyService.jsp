<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="components/header_imports.jsp"%>
</head>
<body class="grey lighten-4">
	<%@include file="components/navbar.jsp"%>

	<div class="container">
		<div class="row blue darken-2 white-text">
			<h1 class="center">Book Flight</h1>
			<form style="padding: 0px 20px 0px 20px;" method="post"
				action="buyingService">
				<!--<div class="input-field col s12 m12 l1">
                    <p class="range-field">
                        <input type="range" id="test5" min="0" max="100" />
                    </p>
                </div>-->
				<!--<p>
                    <label for="spinner">Select a value:</label>
                    <input id="spinner" name="value">
                </p>-->
				<!--                 <div class="input-field col s12 m12 l12">
                    <p style="padding-left:0px; padding-bottom:20px;">
                        <input name="group1" type="radio" id="oneWay" />
                        <label for="oneWay">One Way</label>

                        <input name="group1" type="radio" id="roundTrip" />
                        <label for="roundTrip">Round Trip</label>
                        <input name="group1" type="radio" id="multiCity" />
                        <label for="multiCity">Multi City</label>
                    </p>
                </div> -->
				<div class="input-field col s12 m6 l6">
					<input placeholder="From: City or Airport" id="from" type="text"
						class="validate" name="journeyFrom"> <label for="from">From</label>
				</div>
				<div class="input-field col s12 m6 l6">
					<input placeholder="To: City or Airport" id="to" type="text"
						class="validate" name="journeyTo"> <label for="to">To</label>
				</div>

				<div class="input-field col s6 m6 l3">
					<input type="date" class="datepicker" placeholder="Choose Date"
						name="departDate"> <label for="depart">Depart</label>
				</div>
				<div class="input-field col s6 m6 l3">
					<input type="date" class="datepicker" placeholder="Choose Date"
						name="returnDate"> <label for="return">Return</label>
				</div>
				<div class="input-field col s12 m12 l12 black-text">
					<ul class="collapsible blue" data-collapsible="accordion">
						<li>
							<div class="collapsible-header white">
								Traveller<i class="fa fa-caret-down" aria-hidden="true"></i>
							</div>
							<div class="collapsible-body">
								<div class="row">
									<div class="col s12 m12 l4">
										Adult (12+ yrs)
										<div id="adultIncDec">
											<div class="col l1 m2 s4">
												<img src="resources/img/plus2.png" id="adultUp"
													class="hoverable" />
											</div>
											<div class="col l2 m3 s4">
												<input type="text" value="0" style="padding-left: 22px;"
													name="adultCount" />
											</div>
											<div class="col l1 m2 s4">
												<img src="resources/img/minus-sign-in-a-circle1.png"
													id="adultDown" class="hoverable" />
											</div>
										</div>
									</div>
									<div class="col s12 m12 l4">
										Children (2-12 yrs)
										<div id="childIncDec">
											<div class="col l1 m2 s4">
												<img src="resources/img/plus2.png" id="childUp"
													class="hoverable" />
											</div>
											<div class="col l2 m3 s4">
												<input type="text" value="0" style="padding-left: 22px;"
													name="childCount" />
											</div>
											<div class="col l1 m2 s4">
												<img src="resources/img/minus-sign-in-a-circle1.png"
													id="childDown" class="hoverable" />
											</div>
										</div>
									</div>
									<div class="col s12 m12 l4">
										Infant(s) (below 2 yrs)
										<div id="infantIncDec">
											<div class="col l1 m2 s4">
												<img src="resources/img/plus2.png" id="infantUp"
													class="hoverable" />
											</div>
											<div class="col l2 m3 s4">
												<input type="text" value="0" style="padding-left: 22px;"
													name="infantCount" />
											</div>
											<div class="col l1 m2 s4">
												<img src="resources/img/minus-sign-in-a-circle1.png"
													id="infantDown" class="hoverable" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<!--<select>
      <option value="" disabled selected>Travellers</option>
      <option value="1">Option 1</option>
      <option value="2">Option 2</option>
      <option value="3">Option 3</option>
    </select>
                    <label>Service 1</label>-->
				</div>

				<div class="input-field col s6 m3 l3">
					<select name="travelId">
						<option value="" disabled selected>Class</option>
						<c:forEach var="tClass" items="${travelClassList}">
							<option value="${tClass.classId }">${tClass.className }</option>
						</c:forEach>
					</select> <label>Service 2</label>
				</div>

				<div class="input-field col s6 m6 l4 offset-l5">
					<input type="submit"
						class="btn waves-effect waves-light black darken-4">
					<!--             <button class="btn waves-effect waves-light black darken-4" type="submit">
           Submit
           <i class="material-icons right">send</i>
         </button> -->
				</div>
			</form>
		</div>
	</div>
	<%@include file="components/footer.jsp"%>

</body>

</html>