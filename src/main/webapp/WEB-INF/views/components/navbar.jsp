<div onclick="topFunction()" id="myBtn">
	<img src="resources/img/arrow-up.png" alt="scroll up" />
</div>
<nav class="teal accent-4">
	<div class="navbar-fixed">
		<div class="nav-wrapper">
			<a href="index.html" class="brand-logo hide-on-med-and-down"><img
				class="responsive-img" src="resources/img/travel2.png"> </a> <a
				href="index.html" class="brand-logo hide-on-large-only left"><img
				class="responsive-img" src="resources/img/travel2.png"
				style="padding-left: 25px;"></a> <span
				style="padding-left: 80px; font-size: 24px;"><a
				href="index.html" class="white-text"> Travel Lounge </a></span> <a href="#"
				data-activates="mobile-demo" class="button-collapse"
				style="padding-left: 0px; margin-left: 0px;"><i
				class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
				<li>
					<form style="width: 400px; height: 64px;">
						<div class="input-field teal">
							<input id="search" type="search" placeholder="search..." required>
							<label class="label-icon" for="search"><i
								class="material-icons">search</i></label> <i class="material-icons">close</i>
						</div>
					</form>
				</li>
				<li><a href="#"
					class="waves-effect waves-light btn teal accent-4 "
					style="padding: 0px 5px 4px 5px; height: 64px; margin-right: 0px;"><i
						class="fa fa-user"></i> User</a></li>
				<li><a href="#"
					class="waves-effect waves-light btn teal accent-4"
					style="padding: 0px 5px 4px 5px; height: 64px;"><i
						class="fa fa-sign-out"></i> Logout</a></li>
			</ul>
			<ul id="mobile-demo"
				class="side-nav teal accent-4 hide-on-large-only">
				<!-- <ul class="tabs"> -->
				<h3 class="center white-text">
					<a href="index.html" class="white-text">Travel Lounge </a>
				</h3>
				<li><a href="vendorPage" class="white-text">Vendors</a></li>
				<li><a href="service" class="white-text">Services</a></li>
				<li><a href="mapService" class="white-text">Map Services</a></li>
				<li><a href="buyService" class="white-text">Buy a Service</a></li>
				<li><a href="sellServicePage" class="white-text">Sell a
						Service</a></li>
				<!-- </ul> -->
			</ul>
		</div>
	</div>
</nav>
<ul id="slide-out"
	class="side-nav fixed  teal accent-4 hide-on-med-and-down">
	<!-- <ul class="tabs"> -->
	<h4 class="center teal accent-4 white-text"
		style="margin-top: 0px; padding-bottom: 14px; padding-top: 14px;">
		<a href="index.html" class="white-text">Travel Lounge </a>
	</h4>
	<li><a href="vendorPage" class="white-text">Vendors</a></li>
	<li><a href="service" class="white-text">Services</a></li>
	<li><a href="mapService" class="white-text">Map Services</a></li>
	<li><a href="buyService" class="white-text">Buy a Service</a></li>
	<li><a href="sellServicePage" class="white-text">Sell a Service</a></li>
	<!-- </ul> -->
</ul>
<a href="#" data-activates="slide-out"
	class="button-collapse hide-on-med-and-down"><i
	class="material-icons black">menu</i></a>