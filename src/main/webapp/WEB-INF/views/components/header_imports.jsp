<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title><c:out value="${title}"></c:out></title>
  <link rel="shortcut icon" href="resources/img/travel.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Compiled and minified JavaScript -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
  <link href="resources/css/font-awesome.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="resources/css/font-awesome.css" rel="stylesheet">
  <link href="resources/css/style.css" rel="stylesheet">
  <script src="webapp/resources/js/typed.js" type="text/javascript"></script>
  <script type="text/javascript" src="webapp/resources/js/script.js"></script>
  