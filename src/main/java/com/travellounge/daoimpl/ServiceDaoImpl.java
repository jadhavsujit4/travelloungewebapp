package com.travellounge.daoimpl;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.travellounge.dao.ServiceDao;
import com.travellounge.entity.Service;
import com.travellounge.entity.Vendor;
import com.travellounge.serviceimpl.VendorServiceImpl;


@Component
@Repository("serviceDao")
public class ServiceDaoImpl implements ServiceDao{

	@Autowired
	SessionFactory sessionFactory;
	
	
	@Transactional
	@Override
	public Service addService(Service service) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(service);
	//	System.out.println(viewAllServices());
		return service;
	}

	@Transactional
	@Override
	public Service deleteService(long serviceId) {
		String hql = "from Service where serviceId='" + serviceId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Service> details = (List<Service>) query.list();
		if (details != null && !details.isEmpty()) {
			sessionFactory.getCurrentSession().delete(details.get(0));
			return details.get(0);
		}

		return null;

	}

	@Transactional
	@Override
	public Service updateService(long serviceId) {
		String hql = "from Service where serviceId='" + serviceId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Service> details = (List<Service>) query.list();
		if (details != null && !details.isEmpty()) {
			sessionFactory.getCurrentSession().update(details.get(0));
			return details.get(0);
		}

		return null;
	}

	@Transactional
	@Override
	public Service viewServiceById(long serviceId) {
		String hql = "from Service where serviceId='" + serviceId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Service> service = (List<Service>) query.list();
		if (service != null && !service.isEmpty()) {
			return service.get(0);
		}
		return null;
	}

	
	@Override
	public List<Service> viewAllServices() {
		// TODO Auto-generated method stub
		System.out.println("in chking method");
		String hql = "FROM Service ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Service> serviceList = (List<Service>) query.list();
		if (serviceList != null && !serviceList.isEmpty()) {
			return serviceList;
		}

		return null;
	}
	
	
	

}
