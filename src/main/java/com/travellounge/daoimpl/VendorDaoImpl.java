package com.travellounge.daoimpl;

import java.util.ArrayList;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SharedSessionContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Constants;
import com.travellounge.dao.VendorDao;
import com.travellounge.entity.Address;
import com.travellounge.entity.Contact;
import com.travellounge.entity.TransactionInfo;
import com.travellounge.entity.TransactionMaster;
import com.travellounge.entity.TransactionStatus;
import com.travellounge.entity.TravelClass;
import com.travellounge.entity.TravellerCategory;
import com.travellounge.entity.TravellerInformation;
import com.travellounge.entity.Vendor;
import com.travellounge.entity.birthdate;

@Component
@Repository("vendordao")
public class VendorDaoImpl implements VendorDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public Vendor registerVendor(Vendor vendor,Contact contact, Address address) {
		// TODO Auto-generated method stub
		System.out.println("in dao for save");
		/*
		 * vendor.setVendorName("suwerjit");
		 * vendor.setVendorAddress("chinsdsdchpokli");
		 * vendor.setVendorContact("78963454123");
		 */
		
		sessionFactory.getCurrentSession().save(contact);
		sessionFactory.getCurrentSession().save(address);
		vendor.setContact(contact);
		vendor.setAddress(address);
		sessionFactory.getCurrentSession().save(vendor);
		System.out.println(viewAllVendors().size());

		System.out.println("after save in dao");
		return vendor;
	}

	@Override
	public Vendor viewVendor(long vendorId) {
		String hql = "from Vendor where vendorId='" + vendorId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Vendor> details = (List<Vendor>) query.list();
		if (details != null && !details.isEmpty()) {
			return details.get(0);
		}
		return null;
	}

	@Override
	public Vendor deleteVendor(long vendorId) {
		String hql = "from Vendor where vendorId='" + vendorId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Vendor> details = (List<Vendor>) query.list();
		if (details != null && !details.isEmpty()) {
			sessionFactory.getCurrentSession().delete(details.get(0));
			return details.get(0);
		}

		return null;

	}

	@Override
	public Vendor updateVendor(long vendorId) {
		String hql = "from Vendor where vendorId='" + vendorId + "'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Vendor> details = (List<Vendor>) query.list();
		if (details != null && !details.isEmpty()) {
			sessionFactory.getCurrentSession().update(details.get(0));
			return details.get(0);
		}

		return null;

	}

	@Override
	public List<Vendor> viewAllVendors() {
		// TODO Auto-generated method stub
		String hql = "from Vendor";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Vendor> vendorList = (List<Vendor>) query.list();
		if (vendorList != null && !vendorList.isEmpty()) {
			return vendorList;
		}

		return null;
	}

/*	
	String sql = "SELECT * FROM EMPLOYEE";
	SQLQuery query = session.createSQLQuery(sql);
	query.addEntity(Employee.class);
	List results = query.list();*/
	
	
	@Override
	public boolean mapServiceToVwndor(long vendorId, List<Long> serviceIdList) {
		Session session = sessionFactory.openSession();
		/*String sql1 = "SELECT * FROM vendor";
		SQLQuery q =  session.createSQLQuery(sql1);
		q.addEntity(Vendor.class);

		List vendorList = q.list();
		System.out.println(vendorList.size());
		System.out.println("in dao");
*/
		int rowAffected = 0;
		for (int i = 0; i < serviceIdList.size(); i++) {
			String sql = "INSERT INTO vendor_map_services(vendor_id,service_id) VALUES ('" + vendorId + "','"
					+ serviceIdList.get(i) + "')";
			SQLQuery query = session.createSQLQuery(sql);
	        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			rowAffected += query.executeUpdate();
			System.out.println("In for loop "+rowAffected);

		}
		System.out.println(rowAffected);
		if (rowAffected > 0)
			return true;
		return false;

	}

	@Override
	public boolean exists(Vendor vendor) {
		String hql ="from Vendor where UPPER(TRIM(vendorName))='"+vendor.getVendorName().trim().toUpperCase()+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Vendor> vendorList = (List<Vendor>) query.list();
		if(vendorList.isEmpty()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean addBDate(birthdate birth) {
		// TODO Auto-generated method stub
		System.out.println("In dao for bdate");
		System.out.println("bdate:"+birth.toString());
		sessionFactory.getCurrentSession().save(birth);
		System.out.println("bdate:"+birth.toString());

		System.out.println("in dao added");
		return true;
	}

	@Override
	public List<TravelClass> getTravelClass() {
		// TODO Auto-generated method stub
		String hql = "from TravelClass";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<TravelClass> travelClassList = (List<TravelClass>) query.list();
		System.out.println(travelClassList.size());
		System.out.println(travelClassList.get(0));
		System.out.println(travelClassList);
		return travelClassList;
	}

	@Override
	public boolean buyAService(TransactionMaster transactionMaster, TransactionInfo transactionInfo) {
		// TODO Auto-generated method stub
		System.out.println("in dao to buy");
		sessionFactory.getCurrentSession().save(transactionMaster);
		transactionInfo.setTransactionMaster(transactionMaster);
		sessionFactory.getCurrentSession().save(transactionInfo);
		System.out.println("after buying");
		
		return true;
	}

	@Override
	public List<TransactionStatus> getAllStatus() {
		// TODO Auto-generated method stub
		String hql = "from TransactionStatus";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<TransactionStatus> transactionStatusList = (List<TransactionStatus>) query.list();
		System.out.println(transactionStatusList);
		return transactionStatusList;
	}
	
	public String generateTransactionId(){
        String prefix = "ONO";
		String sql = "select count(id) from transaction_master";
		Session session=sessionFactory.openSession();
		SQLQuery query = session.createSQLQuery(sql);
		List<Long> cnt = (List<Long>)query.list();
		System.out.println(cnt.size());
		System.out.println(cnt.get(0));
		
		Long idtry = Long.parseLong(cnt.get(0)+"");
		Long id = idtry+100000000;
		System.out.println(id);

		//long id=rs.getLong(1)+1000000000;
		String generatedId = prefix + id;
        System.out.println("Generated Id: " + generatedId);
        return generatedId;		
	}

	@Transactional
	private List<TransactionMaster> getTmToSell(){
		System.out.println("in tm to sell");
		//TransactionStatus transactionStatus = getTransactionStatusById(Long.parseLong("1"));
		TransactionStatus transactionStatus = getTransactionStatusByName(com.travellounge.utils.Constants.BOUGHT_TRANSACTION_STATUS);
		System.out.println("Trans Into"+transactionStatus);
		String hql = "From TransactionMaster where transactionStatus = "+transactionStatus.getStatusId()+"";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<TransactionMaster> transactionMasterList = (List<TransactionMaster>) query.list();
		System.out.println(transactionMasterList);
		return transactionMasterList;
	}
	
	
	@Override
	public List<TransactionInfo> getAllTransactionInfo() {
		// TODO Auto-generated method stub
		//List<TransactionMaster> transactionMasterList = getTmToSell();
		System.out.println("b4 list");
		//System.out.println(transactionMasterList);
		TransactionStatus boughtTransactionStatus = getTransactionStatusByName(com.travellounge.utils.Constants.BOUGHT_TRANSACTION_STATUS);
		String hql = "from TransactionInfo where transactionMaster in ( Select id from TransactionMaster where transactionStatus = "+boughtTransactionStatus.getStatusId()+" )";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<TransactionInfo> transactionInfoList = (List<TransactionInfo>) query.list();
		System.out.println(transactionInfoList);
		return transactionInfoList;
	}

	@Override
	public TransactionInfo getTransactionById(long infoId) {
		return (TransactionInfo)sessionFactory.getCurrentSession().get(TransactionInfo.class, infoId);


	}

	@Override
	public TravellerCategory getTravellerCategoryById(long categoryId) {
		// TODO Auto-generated method stub
		return (TravellerCategory)sessionFactory.getCurrentSession().get(TravellerCategory.class,categoryId );
	}

	@Override
	public TransactionMaster getTransactionMasterById(long id) {
		// TODO Auto-generated method stub
		return (TransactionMaster)sessionFactory.getCurrentSession().get(TransactionMaster.class, id);
	}

	@Override
	public void saveTraveller(List<TravellerInformation> travellerInformationList) {
		// TODO Auto-generated method stub
		for(int i = 0;i<travellerInformationList.size();i++){
			//TravellerInformation travellerInformation = travellerInformationList.get(0);
			sessionFactory.getCurrentSession().save(travellerInformationList.get(i));
		}
		
	}

	@Override
	public TransactionMaster changeServiceStatus(TransactionMaster transactionMaster) {
		// TODO Auto-generated method stub
		TransactionStatus transactionStatus = getTransactionStatusById(Long.parseLong("2"));
		transactionMaster.setTransactionStatus(transactionStatus);
		sessionFactory.getCurrentSession().update(transactionMaster);
		return transactionMaster;
	}
	
	private TransactionStatus getTransactionStatusById(long statusId){
		return (TransactionStatus)sessionFactory.getCurrentSession().get(TransactionStatus.class, statusId);
	}
	
	
	private TransactionStatus getTransactionStatusByName(String transactionName){
		String hql = "From TransactionStatus where transactionName = '"+transactionName+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<TransactionStatus> transactionStatusList = (List<TransactionStatus>) query.list();
		return transactionStatusList.get(0);
	}

	/*
	 * public Customer getData(String userId) { String hql =
	 * "from Customer where userId='" + userId + "'"; Query query =
	 * sessionFactory.getCurrentSession().createQuery(hql);
	 * 
	 * @SuppressWarnings("unchecked") List<Customer> details = (List<Customer>)
	 * query.list(); if (details != null && !details.isEmpty()) {
	 * System.out.println(Constants.RETURN_DATA);
	 * 
	 * } else { System.out.println(Constants.RETURN_EMPTY_DATA); } return
	 * details.get(0);
	 * 
	 * }
	 */

}
