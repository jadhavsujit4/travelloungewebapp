package com.travellounge.service;

import java.util.List;


import org.springframework.transaction.annotation.Transactional;

import com.travellounge.entity.Address;
import com.travellounge.entity.Contact;
import com.travellounge.entity.TransactionInfo;
import com.travellounge.entity.TransactionMaster;
import com.travellounge.entity.TransactionStatus;
import com.travellounge.entity.TravelClass;
import com.travellounge.entity.TravellerCategory;
import com.travellounge.entity.TravellerInformation;
import com.travellounge.entity.Vendor;

public interface VendorServiceInterface {

	public Vendor registerVendor(Vendor vendor, Contact contact, Address address);

	public Vendor deleteVendor(long vendorId);

	public Vendor updateVendor(long vendorId);

	public Vendor viewVendor(long vendorId);

	public List<Vendor> viewAllVendors();

	public boolean mapServiceToVwndor(long vendorId, List<Long> serviceIdList);

	public boolean exists(Vendor vendor);

	public List<TravelClass> getTravelClass();

	public List<TransactionStatus> getAllStatus();

	public boolean buyAService(TransactionMaster transactionMaster, TransactionInfo transactionInfo);
	
	public String generateTransactionId();
	
	public List<TransactionInfo> getAllTransactionInfo();
	
	public TransactionInfo getTransactionById(long infoId);

	public TravellerCategory getTravellerCategoryById(long categoryId);

	public TransactionMaster getTransactionMasterById(long id);

	public void saveTraveller(List<TravellerInformation> travellerInformationList);

	public TransactionMaster changeServiceStatus(TransactionMaster transactionMaster);

}
