package com.travellounge.service;

import java.util.List;


import org.springframework.transaction.annotation.Transactional;

import com.travellounge.entity.Service;

public interface ServiceInterface {

	
	public Service addService(Service service);

	
	public Service deleteService(long serviceId);

	
	public Service updateService(long serviceId);

	
	public Service viewServiceById(long serviceId);
	
	
	public List<Service> viewAllServices();

	
	
}
