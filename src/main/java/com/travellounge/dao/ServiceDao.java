package com.travellounge.dao;

import java.util.List
;

import com.travellounge.entity.Service;

public interface ServiceDao {


	public Service addService(Service service);

	public Service deleteService(long serviceId);

	public Service updateService(long serviceId);

	public Service viewServiceById(long serviceId);
	
	public List<Service> viewAllServices();

}
