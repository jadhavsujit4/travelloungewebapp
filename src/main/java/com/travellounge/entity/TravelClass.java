package com.travellounge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "travel_class")
@Component
public class TravelClass {
	
	@Id
	@Column(name = "class_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long classId;
	
	@Column(name = "class_name")
	private String className;
	
	@Column(name = "class_details")
	private String classDetails;
	
	@ManyToOne
	@JoinColumn(name="service_id")
	private Service service;

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassDetails() {
		return classDetails;
	}

	public void setClassDetails(String classDetails) {
		this.classDetails = classDetails;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "TravelClass [classId=" + classId + ", className=" + className + ", classDetails=" + classDetails
				+ ", service=" + service + "]";
	}
	
	

}
