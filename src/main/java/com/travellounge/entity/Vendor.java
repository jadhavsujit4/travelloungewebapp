package com.travellounge.entity;

import java.util.HashSet;

import java.util.Set;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "vendor")
@Component
public class Vendor implements java.io.Serializable{
	
	
	@Id
	@Column(name = "vendor_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long vendorId;
	
	@Column(name = "vendor_name")
	private String vendorName;
	
	@ManyToOne
	@JoinColumn(name="contact_id")
	private Contact contact;
	
	@ManyToOne
	@JoinColumn(name="address_id")
	private Address address;
	

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "vendor_map_services", joinColumns = { @JoinColumn(name = "vendor_id") }, inverseJoinColumns = { @JoinColumn(name = "service_id") })
	private Set<Service> services = new HashSet<Service>();

	public Set<Service> getServices() {
		return this.services;
	}

	public void setServices(Set<Service> services) {
		this.services = services;
	}

	public long getVendorId() {
		return vendorId;
	}

	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Vendor [vendorId=" + vendorId + ", vendorName=" + vendorName + ", contact=" + contact + ", address="
				+ address + ", services=" + services + "]";
	}

	
	

}
