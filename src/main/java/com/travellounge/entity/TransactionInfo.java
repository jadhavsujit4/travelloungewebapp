package com.travellounge.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Table(name="transaction_info")
@Entity
@Component
public class TransactionInfo {

	/* Id transaction_mast_id from to depert_date return_date adult_count child_Count infant_count
 */
	
	@Id
	@Column(name = "info_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long infoId;
	
	@OneToOne(cascade = CascadeType.ALL)
	private TransactionMaster transactionMaster;
	
	@Column(name="journey_from")
	private String journeyFrom;
	
	@Column(name="journey_to")
	private String journeyTo;
	
	@Column(name="depart_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date departDate;
	
	@Column(name="return_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date returnDate;
	
	@Column(name="adult_count")
	private int adultCount;
	
	@Column(name="child_count")
	private int childCount;
	
	@Column(name="infant_count")
	private int infantCount;
	
	@ManyToOne
	@JoinColumn(name="travel_class")
	private TravelClass travelClass;

	public long getInfoId() {
		return infoId;
	}

	public void setInfoId(long infoId) {
		this.infoId = infoId;
	}

	public TransactionMaster getTransactionMaster() {
		return transactionMaster;
	}

	public void setTransactionMaster(TransactionMaster transactionMaster) {
		this.transactionMaster = transactionMaster;
	}

	public String getJourneyFrom() {
		return journeyFrom;
	}

	public void setJourneyFrom(String journeyFrom) {
		this.journeyFrom = journeyFrom;
	}

	public String getJourneyTo() {
		return journeyTo;
	}

	public void setJourneyTo(String journeyTo) {
		this.journeyTo = journeyTo;
	}

	public Date getDepartDate() {
		return departDate;
	}

	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public TravelClass getTravelClass() {
		return travelClass;
	}

	public void setTravelClass(TravelClass travelClass) {
		this.travelClass = travelClass;
	}

	@Override
	public String toString() {
		return "TransactionInfo [infoId=" + infoId + ", transactionMaster=" + transactionMaster + ", journeyFrom="
				+ journeyFrom + ", journeyTo=" + journeyTo + ", departDate=" + departDate + ", returnDate=" + returnDate
				+ ", adultCount=" + adultCount + ", childCount=" + childCount + ", infantCount=" + infantCount
				+ ", travelClass=" + travelClass + "]";
	}
	
	
}
