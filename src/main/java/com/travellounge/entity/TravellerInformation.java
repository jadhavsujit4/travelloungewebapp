package com.travellounge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "traveller_info")
@Component
public class TravellerInformation {
	
	@Id
	@Column(name="traveller_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long travellerId;
	
	@Column(name="traveller_name")
	private String travellerName;
	
	@Column(name="age")
	private int age;
	
	@ManyToOne
	@JoinColumn(name="transaction_master_id")
	private TransactionMaster transactionMaster;
	
	@ManyToOne
	@JoinColumn(name="traveller_category")
	private TravellerCategory travellerCategory;

	public long getTravellerId() {
		return travellerId;
	}

	public void setTravellerId(long travellerId) {
		this.travellerId = travellerId;
	}

	public TransactionMaster getTransactionMaster() {
		return transactionMaster;
	}

	public void setTransactionMaster(TransactionMaster transactionMaster) {
		this.transactionMaster = transactionMaster;
	}

	public String getTravellerName() {
		return travellerName;
	}

	public void setTravellerName(String travellerName) {
		this.travellerName = travellerName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public TravellerCategory getTravellerCategory() {
		return travellerCategory;
	}

	public void setTravellerCategory(TravellerCategory travellerCategory) {
		this.travellerCategory = travellerCategory;
	}

	@Override
	public String toString() {
		return "TravellerInformation [travellerId=" + travellerId + ", transactionMaster=" + transactionMaster
				+ ", travellerName=" + travellerName + ", age=" + age + ", travellerCategory=" + travellerCategory
				+ "]";
	}
	
	
	
	

}
