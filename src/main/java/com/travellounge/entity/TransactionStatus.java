package com.travellounge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table(name="transaction_status")
@Entity
@Component
public class TransactionStatus {
	
	@Id
	@Column(name = "status_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long statusId;
	
	@Column(name = "tName")
	private String transactionName;

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	@Override
	public String toString() {
		return "TransactionStatus [statusId=" + statusId + ", transactionName=" + transactionName + "]";
	}
	
	

}
