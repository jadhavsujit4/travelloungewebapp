package com.travellounge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "contact")
@Component

public class Contact  {
	

	@Id
	@Column(name = "contact_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long contactId;
	
	@Column(name = "email_id")
	private String emailId;
	
	@Column(name = "mobile_no")
	private String mobileNumber;

	@Column(name = "tel_no")
	private String telephoneNumber;

	@Column(name = "fax_no")
	private String faxNumber;

	@Column(name = "website_url")
	private String websiteUrl;

	

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", emailId=" + emailId + ", mobileNumber=" + mobileNumber
				+ ", telephoneNumber=" + telephoneNumber + ", faxNumber=" + faxNumber + ", websiteUrl=" + websiteUrl
				+ "]";
	}
	
	
	
	

}