package com.travellounge.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "services")
@Component
public class Service {

	@Id
	@Column(name = "service_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long serviceId;

	@Column(name = "service_name")
	private String serviceName;

	@Column(name = "service_details")
	private String serviceDetails;

	
	@ManyToMany(mappedBy = "services")
	private Set<Vendor> vendors = new HashSet<Vendor>();

	
	public Set<Vendor> getVendors() {
		return vendors;
	}

	public void setVendors(Set<Vendor> vendors) {
		this.vendors = vendors;
	}

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDetails() {
		return serviceDetails;
	}

	public void setServiceDetails(String serviceDetails) {
		this.serviceDetails = serviceDetails;
	}

	@Override
	public String toString() {
		return "Service [serviceId=" + serviceId + ", serviceName=" + serviceName + ", serviceDetails=" + serviceDetails
				+ "]";
	}
}
