package com.travellounge.controller;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.travellounge.entity.Service;
import com.travellounge.entity.Vendor;
import com.travellounge.service.ServiceInterface;
import com.travellounge.serviceimpl.VendorServiceImpl;
import com.travellounge.service.VendorServiceInterface;

@Controller
public class ServiceController {
	
	@Autowired
	ServiceInterface service;
	
	@Autowired
	Service serviceEntity;
	
	
	@RequestMapping("/service")
	public ModelAndView showServicePage(){
		
		return new ModelAndView("service");
		
	}

	@RequestMapping("/addService")
	public ModelAndView addServicePage(){
		
		return new ModelAndView("addService");
		
	}
	
	@RequestMapping("/addingService")
	public ModelAndView registerVendor(@ModelAttribute  Service serviceEntity, HttpSession session, HttpServletRequest request) {
		System.out.println("before save in controller");
		service.addService(serviceEntity);
		System.out.println(service.viewAllServices().size());
		System.out.println("after save in controler");
		return new ModelAndView("service");
		
	}

	
	
/*	@Autowired
	VendorServiceInterface vendorService;
	
	public VendorController() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public VendorController(VendorServiceImpl service) {
		// TODO Auto-generated constructor stub
		this.vendorService = service;
	}
	
	@Autowired
	VendorDao dao;
	
	@Autowired
	Vendor vendor;
	
	@RequestMapping("/")
	public ModelAndView indexPage(){
		
		return new ModelAndView("registerVendor");
		
	}

	@RequestMapping("/registerVendor")
	public ModelAndView registerVendor(@ModelAttribute Vendor vendor, HttpSession session, HttpServletRequest request) {
		System.out.println("before save in controller");
		vendorService.registerVendor(vendor);
		System.out.println("after save in controler");
		return new ModelAndView("success");
		
	}
*/

}
