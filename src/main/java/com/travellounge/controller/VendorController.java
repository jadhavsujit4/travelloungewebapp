package com.travellounge.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.travellounge.entity.Address;
import com.travellounge.entity.Contact;
import com.travellounge.entity.TransactionInfo;
import com.travellounge.entity.TransactionMaster;
import com.travellounge.entity.TravellerInformation;
import com.travellounge.entity.Vendor;
import com.travellounge.service.ServiceInterface;
import com.travellounge.serviceimpl.VendorServiceImpl;
import com.travellounge.service.VendorServiceInterface;

@Controller
public class VendorController {

	@Autowired
	VendorServiceInterface vendorService;

	@Autowired
	ServiceInterface serviceI;


	/*
	 * @Autowired VendorDao dao;
	 */

	@Autowired
	Vendor vendor;

	@RequestMapping("vendorPage")
	public ModelAndView vendorPage() {
		return new ModelAndView("vendor");

	}

	@RequestMapping("addVendor")
	public ModelAndView addVendorPage() {
		return new ModelAndView("addVendor");

	}
	
	@RequestMapping("/mapService")
	public ModelAndView servicePage(Model model) {
		model.addAttribute("vendorList", vendorService.viewAllVendors());
		model.addAttribute("serviceList", serviceI.viewAllServices());
		return new ModelAndView("vendorMapService");

	}
	
	
	
	@RequestMapping("/addingVendor")
	public ModelAndView registerVendor(@ModelAttribute Vendor vendor,@ModelAttribute Contact contact,@ModelAttribute Address address, HttpSession session, HttpServletRequest request) {
		System.out.println("before save in controller");
		if(vendorService.exists(vendor)){
			//System.out.println("vendor will be aded");
			vendorService.registerVendor(vendor,contact,address);

		}
		else
			System.out.println("not addes");
/*		System.out.println("after save in controler");
*/		return new ModelAndView("success");

	}

	@RequestMapping("/serviceMapPage")
	public ModelAndView serviceMap(Model model) {
		model.addAttribute("vendorList", vendorService.viewAllVendors());
		model.addAttribute("serviceList", serviceI.viewAllServices());
		return new ModelAndView("vendorMapService");

	}

	@RequestMapping("/mapServiceToVendor")
	public ModelAndView mapServiceToVendor(@RequestParam ("vendorId") long vendorId,@RequestParam ("serviceId") List<Long> serviceIdList){
		/*System.out.println(vendorId);
		System.out.println(serviceIdList.size());*/
		if(vendorService.mapServiceToVwndor(vendorId, serviceIdList));
			return new ModelAndView("success");
	}
	
	@RequestMapping("/datePage")
	public ModelAndView datePage(){
		return new ModelAndView("trydate");

	}

/*	@RequestMapping("/dateAdd")
	public ModelAndView addDate(@ModelAttribute birthdate birthdate){
		System.out.println("in controller for bdate" );
		vendorService.addBDate(birthdate);
		return new ModelAndView("success");

	}*/
	
	@RequestMapping("/buyService")
	public ModelAndView buyServicePage(Model model){
		model.addAttribute("travelClassList", vendorService.getTravelClass());
		return new ModelAndView("buyService");
	} 
	
	@RequestMapping("/buyingService")
	public ModelAndView buyingService(@ModelAttribute TransactionInfo transactionInfo,HttpSession session, HttpServletRequest request){
		/*System.out.println(vendorId);
		System.out.println(serviceIdList.size());*/
		System.out.println("in controller to buy service");
		int travelId = Integer.parseInt(request.getParameter("travelId"));
		System.out.println(travelId);
		TransactionMaster transactionMaster = new TransactionMaster();
		transactionMaster.setTransactionStatus(vendorService.getAllStatus().get(0));
		Vendor vendor = vendorService.viewAllVendors().get(0);
		transactionMaster.setVendor(vendor);
		transactionMaster.setService(serviceI.viewAllServices().get(0));
		transactionMaster.setInsertedBy(vendor.getVendorName());
		Date date = new Date();
	//	System.out.println(dateFormat.format(date));
		transactionMaster.setInsertedDate(date);
		transactionMaster.setTransactionId(vendorService.generateTransactionId());		
		vendorService.buyAService(transactionMaster, transactionInfo);
/*		if(vendorService.mapServiceToVwndor(vendorId, serviceIdList));
*/			return new ModelAndView("success");
	}
	
	@RequestMapping("/sellServicePage")
	public ModelAndView sellPage(HttpSession session){
		session.setAttribute("transactionInfoList", vendorService.getAllTransactionInfo());
		return new ModelAndView("viewServicesToSell");

	}
	
	
	@RequestMapping("/sellThisService")
	public ModelAndView gettingDetailsForServiceSelling(Model model,HttpServletRequest request,HttpSession session){
		Long infoId = Long.parseLong(request.getParameter("id"));
		System.out.println("in sell service cont");
		TransactionInfo transactionInfo = vendorService.getTransactionById(infoId);
		//System.out.println(transactionInfo);
		session.setAttribute("transactionInfo", transactionInfo);
		//session.setAttribute("transactionMaster", vendorService.getTransactionMasterById(transactionInfo.get));
		//System.out.println("in selling service "+infoId);
		return new ModelAndView("sellService");
	}
	
	
	@RequestMapping(name="/sellingService")
	public ModelAndView sellingService(HttpServletRequest request,HttpSession session){
		//System.out.println("selling");
		TransactionInfo transactionInfo = (TransactionInfo)session.getAttribute("transactionInfo");
		Integer adultCount=transactionInfo.getAdultCount(),childCount=transactionInfo.getChildCount(),infantCount=transactionInfo.getInfantCount();
		TransactionMaster transactionMaster = transactionInfo.getTransactionMaster();
		List<TravellerInformation> travellerInformationList =new ArrayList<TravellerInformation>();
		TravellerInformation travellerInformation = new TravellerInformation();
		for(int i=1;i<=adultCount;i++){
			System.out.println("in adult for");
			travellerInformation.setTravellerName(request.getParameter("adult_"+i+"_name"));
			travellerInformation.setAge(Integer.parseInt(request.getParameter("adult_"+i+"_age")));
			travellerInformation.setTravellerCategory(vendorService.getTravellerCategoryById(Long.parseLong("1")));
			travellerInformation.setTransactionMaster(transactionMaster);
			travellerInformationList.add(travellerInformation);

		}
		System.out.println(travellerInformation);
		for(int i=1;i<=childCount;i++){
			travellerInformation.setTravellerName(request.getParameter("child_"+i+"_name"));
			travellerInformation.setAge(Integer.parseInt(request.getParameter("child_"+i+"_age")));
			travellerInformation.setTravellerCategory(vendorService.getTravellerCategoryById(Long.parseLong("2")));
			travellerInformation.setTransactionMaster(transactionMaster);
			travellerInformationList.add(travellerInformation);

		}
		for(int i=1;i<=infantCount;i++){
			travellerInformation.setTravellerName(request.getParameter("infant_"+i+"_name"));
			travellerInformation.setAge(Integer.parseInt(request.getParameter("infant_"+i+"_age")));
			travellerInformation.setTravellerCategory(vendorService.getTravellerCategoryById(Long.parseLong("3")));
			travellerInformation.setTransactionMaster(transactionMaster);
			travellerInformationList.add(travellerInformation);

		}
		
		
		vendorService.saveTraveller(travellerInformationList);
		vendorService.changeServiceStatus(transactionMaster);
		
		
		/**
		 * Traveller Info
		 * master Id
		 * master Id Name gender age adult
		 */
		
		
		
		return new ModelAndView("success");
	}
	
	

}



