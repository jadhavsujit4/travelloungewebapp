package com.travellounge.serviceimpl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.travellounge.dao.ServiceDao;
import com.travellounge.entity.Service;
import com.travellounge.service.ServiceInterface;

@org.springframework.stereotype.Service
public class ServiceImpl implements ServiceInterface{
	
	@Autowired
	ServiceDao serviceDao;
	
	@Transactional
	public Service addService(Service service){
		return serviceDao.addService(service);
	}

	@Transactional
	public Service deleteService(long serviceId){
		return serviceDao.deleteService(serviceId);
	}

	@Transactional
	public Service updateService(long serviceId){
		return serviceDao.updateService(serviceId);
	}

	@Transactional
	public Service viewServiceById(long serviceId){
		return serviceDao.viewServiceById(serviceId);
	}
	
	@Transactional
	public List<Service> viewAllServices(){
		return serviceDao.viewAllServices();
	}

	

}
