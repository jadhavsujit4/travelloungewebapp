package com.travellounge.serviceimpl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.travellounge.dao.VendorDao;
import com.travellounge.daoimpl.VendorDaoImpl;
import com.travellounge.entity.Address;
import com.travellounge.entity.Contact;
import com.travellounge.entity.TransactionInfo;
import com.travellounge.entity.TransactionMaster;
import com.travellounge.entity.TransactionStatus;
import com.travellounge.entity.TravelClass;
import com.travellounge.entity.TravellerCategory;
import com.travellounge.entity.TravellerInformation;
import com.travellounge.entity.Vendor;
import com.travellounge.entity.birthdate;
import com.travellounge.service.VendorServiceInterface;

@Component
@Service("vendorService")
@Qualifier("vendorService")
public class VendorServiceImpl implements VendorServiceInterface{
	
	@Autowired
	VendorDao vendordao;
		
	public VendorServiceImpl() {
		// TODO Auto-generated constructor stub
		vendordao = new VendorDaoImpl();
	}
	
	@Transactional
	public Vendor registerVendor(Vendor vendor,Contact contact, Address address) {
		System.out.println("in servoce");
		vendordao.registerVendor(vendor,contact,address);
		return vendor;
	}
	
	@Transactional
	public Vendor deleteVendor(long vendorId){
		Vendor vendor = vendordao.deleteVendor(vendorId);//pravasi ind sainath industry
		return vendor;
	}

	@Transactional
	public Vendor updateVendor(long vendorId){
		Vendor vendor = vendordao.updateVendor(vendorId);
		return vendor;
	}

	
	@Transactional
	public Vendor viewVendor(long vendorId){
		Vendor vendor = vendordao.viewVendor(vendorId);
		return vendor;
	}

	@Transactional
	public List<Vendor> viewAllVendors() {
		// TODO Auto-generated method stub
		return vendordao.viewAllVendors();
	}

	@Override
	@Transactional
	public boolean mapServiceToVwndor(long vendorId, List<Long> serviceIdList) {
		// TODO Auto-generated method stub
		return vendordao.mapServiceToVwndor(vendorId, serviceIdList);
	}

	@Override
	@Transactional
	public boolean exists(Vendor vendor) {
		// TODO Auto-generated method stub
		return vendordao.exists(vendor);
	}



	@Override
	@Transactional
	public List<TravelClass> getTravelClass() {
		// TODO Auto-generated method stub
		return vendordao.getTravelClass();
	}

	@Override
	@Transactional
	public List<TransactionStatus> getAllStatus() {
		// TODO Auto-generated method stub
		return vendordao.getAllStatus();
	}

	@Override
	@Transactional
	public boolean buyAService(TransactionMaster transactionMaster, TransactionInfo transactionInfo) {
		// TODO Auto-generated method stub
		return vendordao.buyAService(transactionMaster, transactionInfo);
	}

	@Override
	@Transactional
	public String generateTransactionId() {
		// TODO Auto-generated method stub
		return vendordao.generateTransactionId();
	}

	@Override
	@Transactional
	public List<TransactionInfo> getAllTransactionInfo() {
		// TODO Auto-generated method stub
		return vendordao.getAllTransactionInfo();
	}

	@Override
	@Transactional
	public TransactionInfo getTransactionById(long infoId) {
		// TODO Auto-generated method stub
		return vendordao.getTransactionById(infoId);
	}

	@Override
	@Transactional
	public TravellerCategory getTravellerCategoryById(long categoryId) {
		// TODO Auto-generated method stub
		return vendordao.getTravellerCategoryById(categoryId);
	}

	@Override
	@Transactional
	public TransactionMaster getTransactionMasterById(long id) {
		// TODO Auto-generated method stub
		return vendordao.getTransactionMasterById(id);
	}

	@Override
	@Transactional
	public void saveTraveller(List<TravellerInformation> travellerInformationList) {
		// TODO Auto-generated method stub
		vendordao.saveTraveller(travellerInformationList);
		
	}

	@Override
	@Transactional
	public TransactionMaster changeServiceStatus(TransactionMaster transactionMaster) {
		// TODO Auto-generated method stub
		return vendordao.changeServiceStatus(transactionMaster);
	}


}
