 $(document).ready(function() {
     $(".button-collapse").sideNav();
     $('.collapsible').collapsible();
     $('.modal').modal();
     $('select').material_select();
     $('.datepicker').pickadate({
         selectMonths: true, // Creates a dropdown to control month
         selectYears: 15,
         format: 'yyyy-mm-dd'
         // Creates a dropdown of 15 years to control year
     });
     $("#adultUp").on('click', function() {
         $("#adultIncDec input").val(parseInt($("#adultIncDec input").val()) + 1);
     });

     $("#adultDown").on('click', function() {
         $("#adultIncDec input").val(parseInt($("#adultIncDec input").val()) - 1);
     });

     $("#childUp").on('click', function() {
         $("#childIncDec input").val(parseInt($("#childIncDec input").val()) + 1);
     });

     $("#childDown").on('click', function() {
         $("#childIncDec input").val(parseInt($("#childIncDec input").val()) - 1);
     });
     $("#infantUp").on('click', function() {
         $("#infantIncDec input").val(parseInt($("#infantIncDec input").val()) + 1);
     });

     $("#infantDown").on('click', function() {
         $("#infantIncDec input").val(parseInt($("#infantIncDec input").val()) - 1);
     });
 });
 window.onscroll = function() { scrollFunction() };

 function scrollFunction() {
     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
         document.getElementById("myBtn").style.display = "block";
     } else {
         document.getElementById("myBtn").style.display = "none";
     }
 }
 // When the user clicks on the button, scroll to the top of the document
 function topFunction() {
     document.body.scrollTop = 0; // For Chrome, Safari and Opera 
     document.documentElement.scrollTop = 0; // For IE and Firefox
 }